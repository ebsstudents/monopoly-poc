﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Monopoly_POC.Entities
{
    [Serializable]
    public class MonopolyGame
    {
        public List<Player> playerList = new List<Player>();
        public List<Cell> board = new List<Cell>();
        public Dice dice;


        public void ReadPlayers(string filePath)
        {
            using (StreamReader stream = new StreamReader("players.txt"))
            {
                XmlSerializer xml = new XmlSerializer(typeof(List <Player>));
                playerList = (List <Player>) xml.Deserialize(stream);
            }

        }
    }
}
