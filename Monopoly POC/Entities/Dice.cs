﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly_POC.Entities
{
    public class Dice
    {
        static Random dice = new Random();
        static public int Roll()
        {
            return dice.Next(1, 7);
        }
    }
}
