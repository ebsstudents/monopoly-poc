﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Monopoly_POC.Constants;
using System.Drawing;

namespace Monopoly_POC.Entities
{
   [Serializable]
    public class Cell
    {
       Color color;

        string name;

        public Color Color
        {
            get
            {
                return this.color;
            }
            set
            {
                this.color = value;
            }
        }

        public int Y
        {
            get
            {
                return this.y;
            }
            set
            {
                this.y = value;
            }
        }

        public int X
        {
            get
            {
                return this.x;
            }
            set
            {
                this.x = value;
            }
        }

        public PlayerTypes? Owner
        {
            get
            {
                return this.owner;
            }
            set
            {
                this.owner = value;
            }
        }

        public int Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }

        public int Rent
        {
            get
            {
                return this.rent;
            }
            set
            {
                this.rent = value;
            }
        }

        public int Cost
        {
            get
            {
                return this.cost;
            }
            set
            {
                this.cost = value;
            }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        int cost;
        int rent;
        int position;
        Constants.PlayerTypes? owner;
        int x, y;
    }
}
