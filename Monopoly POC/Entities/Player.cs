﻿using Monopoly_POC.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monopoly_POC.Entities
{
    [Serializable]
    public class Player
    {
        #region private members
            private string name;
            private PlayerTypes type;

            
        #endregion 

       #region public members

            public string Name
            {
                get { return name; }
                set { name = value; }
            }

            public PlayerTypes Type
            {
                get { return type; }
                set { type = value; }
            }
            private int money;

            public int Money
            {
                get { return money; }
                set { money = value; }
            }

       #endregion
    }
}
