﻿namespace Monopoly_POC
{
    partial class CellView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Nume = new System.Windows.Forms.Label();
            this.Cost = new System.Windows.Forms.Label();
            this.NumeEdit = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Nume
            // 
            this.Nume.AutoSize = true;
            this.Nume.Font = new System.Drawing.Font("Tw Cen MT Condensed", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nume.Location = new System.Drawing.Point(13, 0);
            this.Nume.Name = "Nume";
            this.Nume.Size = new System.Drawing.Size(82, 31);
            this.Nume.TabIndex = 0;
            this.Nume.Text = "RAMADA";
            this.Nume.DoubleClick += new System.EventHandler(this.Nume_DoubleClick);
            // 
            // Cost
            // 
            this.Cost.AutoSize = true;
            this.Cost.Location = new System.Drawing.Point(33, 44);
            this.Cost.Name = "Cost";
            this.Cost.Size = new System.Drawing.Size(31, 13);
            this.Cost.TabIndex = 1;
            this.Cost.Text = "1200";
            // 
            // NumeEdit
            // 
            this.NumeEdit.Location = new System.Drawing.Point(3, 21);
            this.NumeEdit.Name = "NumeEdit";
            this.NumeEdit.Size = new System.Drawing.Size(91, 20);
            this.NumeEdit.TabIndex = 2;
            this.NumeEdit.Visible = false;
            this.NumeEdit.Enter += new System.EventHandler(this.NumeEdit_Enter);
            this.NumeEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NumeEdit_KeyDown);
            this.NumeEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NumeEdit_KeyPress);
            this.NumeEdit.Leave += new System.EventHandler(this.NumeEdit_Leave);
            // 
            // CellView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.NumeEdit);
            this.Controls.Add(this.Cost);
            this.Controls.Add(this.Nume);
            this.Name = "CellView";
            this.Size = new System.Drawing.Size(98, 98);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label Nume;
        public System.Windows.Forms.Label Cost;
        private System.Windows.Forms.TextBox NumeEdit;
    }
}
