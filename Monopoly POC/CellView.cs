﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly_POC
{
    public partial class CellView : UserControl
    {
        public CellView()
        {
            InitializeComponent();
        }

        private void Nume_DoubleClick(object sender, EventArgs e)
        {
            Nume.Visible = false;
            NumeEdit.Visible = true;
        }

        private void NumeEdit_Enter(object sender, EventArgs e)
        {
            NumeEdit.Text = Nume.Text;

        }

        private void NumeEdit_Leave(object sender, EventArgs e)
        {

        }

        private void NumeEdit_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void NumeEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Nume.Text = NumeEdit.Text;
             Nume.Visible = true;
            NumeEdit.Visible = false;
            }
        }
    }
}
