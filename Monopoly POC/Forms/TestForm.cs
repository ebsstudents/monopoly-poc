﻿using Monopoly_POC.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using Monopoly_POC.Constants;
using System.Xml.Serialization;



namespace Monopoly_POC
{
    public partial class TestForm : Form
    {

        MonopolyGame game = new MonopolyGame();

        public TestForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            game.ReadPlayers("players.txt");
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void TestForm_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            game.board[0].Color = Color.Teal;
            game.board[1].Color = Color.Chartreuse;
            game.board[3].Color = Color.Gold;
           game.board[2].Color = Color.DeepSkyBlue;

            using (StreamWriter file = new StreamWriter("board.txt"))
            {
                XmlSerializer xml = new XmlSerializer(typeof(List<Cell>));
                xml.Serialize(file, game.board);
            }

            dataGridView1.DataSource = game.board;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (StreamReader file = new StreamReader("board.txt"))
            {
                XmlSerializer xml = new XmlSerializer(typeof(List<Cell>));
                game.board = (List <Cell>) xml.Deserialize(file);
            }
            dataGridView1.DataSource = game.board;

            foreach (var cell in game.board)
            {
                CellView cellView = new CellView();

                cellView.Nume.Text = cell.Name;
                cellView.Top = cell.X;
                cellView.Left = cell.Y;
                cellView.BackColor = cell.Color;
                cellView.Cost.Text = cell.Cost.ToString();

                Controls.Add(cellView);
            }
        }
    }
}
