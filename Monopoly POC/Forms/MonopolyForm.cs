﻿using Monopoly_POC.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monopoly_POC.Forms
{
    public partial class MonopolyForm : Form
    {
        MonopolyGame monopoly = new MonopolyGame();
        
        public MonopolyForm()
        {
            InitializeComponent();
        }

        public MonopolyForm(List <Player> playerList)
        {
            monopoly.playerList = playerList;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int counter = 0;
            foreach (var cell in monopoly.board)
            {
                if (cell.Owner == Constants.PlayerTypes.Shoe)
                {
                    counter++;
                }
            }

            monopoly.board.Count(cell => cell.Owner == Constants.PlayerTypes.Shoe);
        }

        private void MonopolyForm_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = monopoly.playerList;
        }
    }
}
