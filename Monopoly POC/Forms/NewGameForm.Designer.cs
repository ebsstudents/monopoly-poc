﻿namespace Monopoly_POC
{
    partial class NewGameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NewGameSplitContainer = new System.Windows.Forms.SplitContainer();
            this.playersGridView = new System.Windows.Forms.DataGridView();
            this.startNewGameButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NewGameSplitContainer)).BeginInit();
            this.NewGameSplitContainer.Panel1.SuspendLayout();
            this.NewGameSplitContainer.Panel2.SuspendLayout();
            this.NewGameSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playersGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // NewGameSplitContainer
            // 
            this.NewGameSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NewGameSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.NewGameSplitContainer.Name = "NewGameSplitContainer";
            this.NewGameSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // NewGameSplitContainer.Panel1
            // 
            this.NewGameSplitContainer.Panel1.Controls.Add(this.playersGridView);
            // 
            // NewGameSplitContainer.Panel2
            // 
            this.NewGameSplitContainer.Panel2.Controls.Add(this.startNewGameButton);
            this.NewGameSplitContainer.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.NewGameSplitContainer_Panel2_Paint);
            this.NewGameSplitContainer.Size = new System.Drawing.Size(452, 306);
            this.NewGameSplitContainer.SplitterDistance = 234;
            this.NewGameSplitContainer.TabIndex = 0;
            // 
            // playersGridView
            // 
            this.playersGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.playersGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playersGridView.Location = new System.Drawing.Point(0, 0);
            this.playersGridView.Name = "playersGridView";
            this.playersGridView.Size = new System.Drawing.Size(452, 234);
            this.playersGridView.TabIndex = 0;
            // 
            // startNewGameButton
            // 
            this.startNewGameButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.startNewGameButton.Location = new System.Drawing.Point(167, 33);
            this.startNewGameButton.Name = "startNewGameButton";
            this.startNewGameButton.Size = new System.Drawing.Size(75, 23);
            this.startNewGameButton.TabIndex = 0;
            this.startNewGameButton.Text = "START";
            this.startNewGameButton.UseVisualStyleBackColor = true;
            this.startNewGameButton.Click += new System.EventHandler(this.startNewGameButton_Click);
            // 
            // NewGameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 306);
            this.Controls.Add(this.NewGameSplitContainer);
            this.Name = "NewGameForm";
            this.Text = "NEW GAME";
            this.Load += new System.EventHandler(this.NewGameForm_Load);
            this.NewGameSplitContainer.Panel1.ResumeLayout(false);
            this.NewGameSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NewGameSplitContainer)).EndInit();
            this.NewGameSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playersGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer NewGameSplitContainer;
        private System.Windows.Forms.DataGridView playersGridView;
        private System.Windows.Forms.Button startNewGameButton;

    }
}

