﻿using System.IO;
using Monopoly_POC.Constants;
using Monopoly_POC.Entities;
using Monopoly_POC.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Monopoly_POC
{
    public partial class NewGameForm : Form
    {
        List<Player> playerList = new List<Player>();

        public NewGameForm()
        {
            InitializeComponent();
        }

        private void startNewGameButton_Click(object sender, EventArgs e)
        {

            playerList.RemoveAll(player => player.Money==0 || player.Name == string.Empty);

            MonopolyForm monopolyForm = new MonopolyForm(playerList);
            monopolyForm.ShowDialog();
        }


        private void InitilazePlayerList()
        {
            //playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Boat });
            //playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Car });
            //playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Plane });
            //playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Shoe });
            //playerList.Add(new Player { Name = "", Money = 1000, Type = PlayerTypes.Tank });

            using (StreamReader stream = new StreamReader("players.txt"))
            {
                XmlSerializer xml = new XmlSerializer(typeof(List <Player>));
                xml.Deserialize(stream);
            }

        }



        private void NewGameForm_Load(object sender, EventArgs e)
        {
            InitilazePlayerList();
            playersGridView.DataSource = playerList;
        }

        private void NewGameSplitContainer_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
